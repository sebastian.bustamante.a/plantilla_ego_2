from django.shortcuts import render
from productos.models import Categoria,Subcategoria,Producto
from django.views.generic import TemplateView
# Create your views here.


class CategoriaView(TemplateView):
    template_name = "categoria.html"
    
    def get_context_data(self, **kwargs):
        kwargs['libros'] = Producto.objects.all()
        return kwargs

class ProductoViewPerson(TemplateView):
    template_name="producto_detalle/p_d.html"
    def get_context_data(self, **kwargs):
        producto_slug=kwargs.get("slug")
        kwargs['libros'] = Producto.objects.get(slug=producto_slug)
        return kwargs
