from django.conf.urls import url
from django.urls import path
from productos.views import CategoriaView,ProductoViewPerson

urlpatterns = [
    path('',CategoriaView.as_view(),name='index'),
    path('<slug:slug>',ProductoViewPerson.as_view(),name='producto'),
]